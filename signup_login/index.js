const express = require("express");
const formidable = require("express-formidable");
const mongoose = require("mongoose");
const app = express();

app.use(formidable());

//Import routes
const userRoutes = require("./routes/User-route");
app.use(userRoutes);

//Import routes
const offerRoutes = require("./routes/Offer-route");
app.use(offerRoutes);

// Connect DB
mongoose.connect("mongodb://localhost:27017/vinted");

//Reject all routes
app.all("*", (req, res) => {
  res.status(404).json({ Message: "Page not found" });
});

//Expose port
app.listen(3000, () => {
  console.log("Server Started");
});
