const express = require("express");
const isAuthenticated = require("../middleware/isAuthenticated");
const router = express.Router();
const cloudinary = require("cloudinary").v2;

const Offer = require("../models/Offer-model");
const User = require("../models/User-model");

router.post("/offer/publish", isAuthenticated, async (req, res) => {
  try {
    // console.log(req.fields);
    // console.log(req.files.picture.path);

    // Destructuring
    const { title, description, price, condition, city, brand, size, color } =
      req.fields;

    const newOffer = new Offer({
      product_name: title,
      product_description: description,
      product_price: price,
      product_details: [
        {
          MARQUE: brand,
        },
        {
          TAILLE: size,
        },
        {
          ÉTAT: condition,
        },
        {
          COULEUR: color,
        },
        {
          EMPLACEMENT: city,
        },
      ],
      owner: req.user,
    });

    // Upload de l'image
    const result = await cloudinary.uploader.upload(req.files.picture.path, {
      folder: `/vinted-pegasus21/offers/${newOffer._id}`,
    });

    newOffer.product_image = result;

    await newOffer.save();

    res.status(200).json(newOffer);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});
