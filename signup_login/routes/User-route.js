const express = require("express");
const { appendFile } = require("fs");
const router = express.Router();

//Import encryption package
const SHA256 = require("crypto-js/sha256");
const encBase64 = require("crypto-js/enc-base64");
const uid2 = require("uid2");
const User = require("../models/User-model");
const Offer = require("../models/Offer-model");

//Declare API routes
//Route to signup
router.post("/user/signup", async (req, res) => {
  try {
    const email = req.fields.email;
    const username = req.fields.username;
    const phone = req.fields.phone;
    const password = req.fields.password;
    // l'email renseigné lors de l'inscription existe déjà dans la base de données
    // le username n'est pas renseigné
    // const existingUsername = await User.findOne({ username: username });
    if (username) {
      const user = await User.findOne({ email: email });

      if (!user) {
        const salt = uid2(16);
        const hash = SHA256(password + salt).toString(encBase64);
        const token = uid2(16);

        // Declare new user
        const newUser = new User({
          email: email,
          account: {
            username: username,
            phone: phone,
          },
          token: token,
          hash: hash,
          salt: salt,
        });

        //Store new user in DB
        await newUser.save();

        //Retrieve all information about User DB
        res.json({
          _id: newUser._id,
          token: newUser.token,
          account: newUser.account,
        });
      } else {
        res.json({ Message: "Access denied email" });
      }
    } else {
      res.json({ Message: "Access denied username" });
    }
  } catch (error) {
    console.error(error.message);
  }
});

router.post("/user/login", async (req, res) => {
  try {
    //check if email exists in DB
    const existingEmail = await User.findOne({ email: req.fields.email });

    if (existingEmail) {
      //Hash password+salt of entries
      const hashExistingEmail = SHA256(
        req.fields.password + existingEmail.salt
      ).toString(encBase64);

      //compare the result of entries with the one in the DB
      if (hashExistingEmail === existingEmail.hash) {
        res.status(200).json({
          _id: existingEmail._id,
          token: existingEmail.token,
          account: existingEmail.account,
        });
      } else {
        res.status(401).json({ Message: "Access Unauthorized" });
      }
      //if your result and hash in DB is similar,you can login if not, your password is wrong
    } else {
      res.status(400).json({ Message: "Bad Request" });
    }
  } catch (error) {
    console.error(error.message);
  }
});

module.exports = router;
