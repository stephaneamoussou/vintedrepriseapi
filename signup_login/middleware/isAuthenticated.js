// const User = require("../models/User-model");

// //Import encryption package
// const SHA256 = require("crypto-js/sha256");
// const encBase64 = require("crypto-js/enc-base64");
// const uid2 = require("uid2");

// const Offer = require("../models/Offer-model");

// const isAuthenticated = async (req, res, next) => {
//   try {
//     //check if email exists in DB
//     const existingEmail = await User.findOne({
//       email: req.fields.email,
//     }).select("_id account");

//     if (existingEmail) {
//       //Hash password+salt of entries
//       const hashExistingEmail = SHA256(
//         req.fields.password + existingEmail.salt
//       ).toString(encBase64);

//       //compare the result of entries with the one in the DB
//       if (hashExistingEmail === existingEmail.hash) {
//         req.existingEmail = existingEmail;
//         next();
//       } else {
//         res.status(404).json({ Message: "Not Found" });
//       }
//       //if your result and hash in DB is similar,you can login if not, your password is wrong
//     } else {
//       res.status(400).json({ Message: "Bad Request" });
//     }
//   } catch (error) {
//     console.error(error.message);
//   }
// };

const User = require("../models/User-model"); // ES5
// import User from "./" // ES6
const Offer = require("../models/Offer-model");

const isAuthenticated = async (req, res, next) => {
  try {
    // Récupérer le Bearer token
    const userToken = req.headers.authorization.replace("Bearer ", "");
    console.log(userToken);
    // Chercher le user qui possède ce token
    const user = await User.findOne({ token: userToken }).select("_id account");
    if (user) {
      // Si ok ===> next()
      req.user = user;
      next();
    } else {
      res.status(401).json({ message: "Unauthorized" });
    }
    // Sinon ===> unauthorized
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

module.exports = isAuthenticated; // ES5
// export default isAuthenticated // ES6
